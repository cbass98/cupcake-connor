package uk.ac.tees.com2060.cupcake;

import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * App Compat Preference Activity.
 *
 * A {@link android.preference.PreferenceActivity} which implements and proxies the necessary calls
 * to be used with AppCompat.
 */
public abstract class AppCompatPreferenceActivity extends PreferenceActivity
{
    private AppCompatDelegate mDelegate;

    /**
     * Initializes activity.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        getDelegate().installViewFactory();
        getDelegate().onCreate(savedInstanceState);
        super.onCreate(savedInstanceState);
    }

    /**
     * Called when activity start-up is complete
     * @param savedInstanceState
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState)
    {
        super.onPostCreate(savedInstanceState);
        getDelegate().onPostCreate(savedInstanceState);
    }

    /**
     * Retrieves a reference to this activity's ActionBar.
     * The support library version of getActionBar().
     * @return SupportActionbar
     */
    public ActionBar getSupportActionBar() {
        return getDelegate().getSupportActionBar();
    }

    /**
     * Sets a Toolbar to act as the ActionBar for this Activity window.
     * @param toolbar
     */
    public void setSupportActionBar(@Nullable Toolbar toolbar)
    {
        getDelegate().setSupportActionBar(toolbar);
    }

    /**
     * Returns a MenuInflater with this context.
     * @return MenuInflater
     */
    @Override
    public MenuInflater getMenuInflater() {
        return getDelegate().getMenuInflater();
    }

    /**
     * Set the activity content from a layout resource
     * @param layoutResID
     */
    @Override
    public void setContentView(@LayoutRes int layoutResID)
    {
        getDelegate().setContentView(layoutResID);
    }

    /**
     * Set the activity content to an explicit view.
     * @param view
     */
    @Override
    public void setContentView(View view) {
        getDelegate().setContentView(view);
    }

    /**
     * Sets the activity content to an explicit view.
     * This view is placed directly into the activity's view hierarchy.
     * @param view
     * @param params
     */
    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params)
    {
        getDelegate().setContentView(view, params);
    }

    /**
     * Adds an additional content view to the activity.
     * Added after any existing ones in the activity
     * @param view
     * @param params
     */
    @Override
    public void addContentView(View view, ViewGroup.LayoutParams params)
    {
        getDelegate().addContentView(view, params);
    }

    /**
     * Called when activity resume is complete
     */
    @Override
    protected void onPostResume()
    {
        super.onPostResume();
        getDelegate().onPostResume();
    }

    /**
     * Changes the title and the colour of the title associated with this activity.
     * @param title
     * @param color
     */
    @Override
    protected void onTitleChanged(CharSequence title, int color)
    {
        super.onTitleChanged(title, color);
        getDelegate().setTitle(title);
    }

    /**
     * Called by the system when the device configuration changes while your activity is running.
     * @param newConfig
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        getDelegate().onConfigurationChanged(newConfig);
    }

    /**
     * Called when no longer visible to the user.
     */
    @Override
    protected void onStop()
    {
        super.onStop();
        getDelegate().onStop();
    }

    /**
     * Performs any final cleanup before an activity is destroyed.
     */
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        getDelegate().onDestroy();
    }

    /**
     * Declare that the options menu has changed, so should be recreated.
     */
    public void invalidateOptionsMenu() {
        getDelegate().invalidateOptionsMenu();
    }

    /**
     * The AppCompatDelegate being used by this Activity.
     * @return mDelegate
     */
    private AppCompatDelegate getDelegate()
    {
        if (mDelegate == null)
        {
            mDelegate = AppCompatDelegate.create(this, null);
        }
        return mDelegate;
    }
}
