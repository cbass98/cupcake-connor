package uk.ac.tees.com2060.cupcake;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * BidDetailsActivity Class
 */
public class BidDetailsActivity extends AppCompatActivity
{
    //Variables used for layout access
    TextView tvBidder, tvBid;
    UserBid bid;
    UserJob job;
    Button btnAccept;

    //Variable for database reference
    DatabaseReference uDatabaseReference, bDatabaseReference, jDatabaseReference;
    private RecyclerView recyclerView;
    private DatabaseReference rDatabaseRef;
    List<Review> reviews = new ArrayList<>();
    private ReviewRecyclerViewAdapter rAdapter;

    /**
     * Initializes Activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        //Sets up the required xml layout file
        setContentView(R.layout.activity_bid_details);

        //Initializes the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Initialize widgets
        tvBidder = (TextView)findViewById(R.id.tv_bidder);
        tvBid = (TextView)findViewById(R.id.tv_bid);
        btnAccept = (Button)findViewById(R.id.btn_accept);

        //Initialize databases
        jDatabaseReference = FirebaseDatabase.getInstance().getReference("job");
        bDatabaseReference = FirebaseDatabase.getInstance().getReference("bid");

        //OnClickListener for button
        btnAccept.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                jDatabaseReference.addValueEventListener(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        //Grabs each element in the relevant database
                        for(DataSnapshot snapshot : dataSnapshot.getChildren())
                        {
                            //Checks if the current job element matches the reference of the current bid
                            if(snapshot.getKey().equals(bid.getJobRef()))
                            {
                                //Grabs the current user job
                                job = snapshot.getValue(UserJob.class);

                                //Sets the status to accepted and sets acceptedBidder to the current bidder
                                //Pushes the element back up to the database
                                job.setStatus("Accepted");
                                job.setAcceptedBidder(bid.getBidder());
                                jDatabaseReference.child(snapshot.getKey()).setValue(job);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {

                    }
                });

                bDatabaseReference.addValueEventListener(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        //Grabs each element in the relevant database
                        for(DataSnapshot snapshot : dataSnapshot.getChildren())
                        {
                            //Checks that the current bid element matches the current displayed bid
                            if(snapshot.getValue(UserBid.class).equals(bid))
                            {
                                //Sets accepted to true and pushes the element back up to the database
                                bid.setAccepted(true);
                                bDatabaseReference.child(snapshot.getKey()).setValue(bid);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {

                    }
                });

                //Goes back to the explore class
                startActivity(new Intent(BidDetailsActivity.this, ExploreActivity.class));
            }
        });

        //Calls getIncomingIntent()
        getIncomingIntent();

    }

    /**
     * Retrieves data sent with the intent in the extra field
     */
    private void getIncomingIntent()
    {
        Log.d(TAG, "getIncomingIntent: checking for incoming intent");

        //Checks if the intent has an extra with the reference bid
        if (this.getIntent().hasExtra("bid"))
        {
            Log.d(TAG, "getIncomingIntent: found intent extra");

            //Grabs the data in the extra
            UserBid bid = (UserBid) this.getIntent().getSerializableExtra("bid");

            //Calls setBid()
            setBid(bid);
        }
    }

    /**
     * Deals with the data sent with the intent
     * @param b
     */
    private void setBid(UserBid b)
    {
        //Store the data sent
        bid = b;

        //Initialize the database reference
        uDatabaseReference = FirebaseDatabase.getInstance().getReference("user");
        uDatabaseReference.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot snapshot)
            {
                //Grabs each element in the relevant database
                for (DataSnapshot dataSnapshot : snapshot.getChildren())
                {
                    //Checks if the user in the database matches the bidder
                    if(dataSnapshot.getKey().equals(bid.getBidder()))
                    {
                        //If they match grabs the User object and sets the textView to the user's name
                        User u = dataSnapshot.getValue(User.class);
                        tvBidder.setText(u.getName());
                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });

        //Initializes the RecyclerView for the current bidder's reviews
        recyclerView = (RecyclerView)findViewById(R.id.recyclerViewReviews);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //Initialize the database reference
        rDatabaseRef = FirebaseDatabase.getInstance().getReference("review");
        rDatabaseRef.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot snapshot)
            {
                //Grabs each element in the relevant database
                for (DataSnapshot dataSnapshot : snapshot.getChildren())
                {
                    //Grab the current review object
                    Review review = dataSnapshot.getValue(Review.class);

                    //Checks that the reviewee is the same as the current bidder
                    if(review.getReviewee().equals(bid.getBidder()))
                    {
                        //Adds the current review to the list of reviews
                        reviews.add(review);
                    }

                }

                //Sets the adapter for the reviews and passes the reviews to the adapter
                rAdapter = new ReviewRecyclerViewAdapter(getApplicationContext(), reviews);
                recyclerView.setAdapter(rAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {


            }
        });

        //Sets the textView to the bid amount
        tvBid.setText("£ " + bid.getBid());

    }
}