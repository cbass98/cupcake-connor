package uk.ac.tees.com2060.cupcake;


import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * ExploreTab1Inbox Fragment
 */
public class ExploreTab1Inbox extends Fragment
{
    private FirebaseAuth mAuth;
    private DatabaseReference mUsersDBRef;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private UsersRecyclerViewAdapter adapter;
    List<User> mUsersList;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_tab1_inbox, container, false);

        //assign firebase authentication
        mAuth = FirebaseAuth.getInstance();
        //initialize the recycler view variables
        mRecyclerView = (RecyclerView)rootView.findViewById(R.id.usersRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        //use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        //grabs users from Firebase
        mUsersList = new ArrayList<>();
        mUsersDBRef = FirebaseDatabase.getInstance().getReference("user");
        mUsersDBRef.addValueEventListener(new ValueEventListener()
        {
            /**
             *Log.d is for the debugger, it is left in in case someone decides to come back to the project at a later date
             * This method gets the User from Firebase
             */
            @Override
            public void onDataChange(DataSnapshot snapshot)
            {
                Log.d(TAG, "onDataChange: Finding users in database");
                for(DataSnapshot dataSnapshot : snapshot.getChildren())
                {

                    User user = dataSnapshot.getValue(User.class);
                    if(!mAuth.getCurrentUser().getUid().equals(user.getId()))
                    {
                        Log.d(TAG, "onDataChange: User found : " + user.getName());
                        mUsersList.add(user);
                    }


                }

                Log.d(TAG, "onCreateView: Setting adapter");
                adapter = new UsersRecyclerViewAdapter(mUsersList, getActivity());
                mRecyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

        return rootView;
    }

}