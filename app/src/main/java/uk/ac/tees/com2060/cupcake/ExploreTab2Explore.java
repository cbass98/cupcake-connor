package uk.ac.tees.com2060.cupcake;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * ExploreTab2Explore Fragment
 */

public class ExploreTab2Explore extends Fragment
{
    //Variable for database reference
    DatabaseReference databaseReference;

    //Variables for layout access
    ProgressDialog progressDialog;
    RecyclerView recyclerView, recyclerView2, recyclerView3;
    JobsRecyclerViewAdapter adapter, adapter2, adapter3;

    //Variable for Firebase access
    FirebaseAuth auth;

    //Variables for object storage
    String id;
    List<UserJob> aList = new ArrayList<>();
    List<UserJob> oList = new ArrayList<>();
    List<UserJob> pList = new ArrayList<>();

    //Variables for location services
    private static final String TAG = "MainActivity";
    private static final String FINE_LOCATION = android.Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = android.Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private static final int ERROR_DIALOG_REQUEST = 9001;
    private FusedLocationProviderClient mfusedLocationProviderClient;
    private Boolean mLocationPermissionGranted = false;
    List<Address> addresses;
    String currentPostalCode;
    Location current = new Location("");
    Location pickup = new Location("");
    Location dropoff = new Location("");
    float range = (float) 16093.4;

    /**
     * Initializes Fragment
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        //Checks if Google Location Services is ok
        if(isServicesOK())
        {
            //If the services are working, checks for location permission and then get's
            //the device location
            getLocationPermission();

        }
        if(mLocationPermissionGranted == true)
        {
            getDeviceLocation();
        }
    }


    /**
     * Creates and returns the view hierarchy associated with fragment
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return rootView
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_tab2_explore, container, false);

        //Initializes the RecyclerView for jobs in the current location
        recyclerView = (RecyclerView)rootView.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        //Initializes the RecyclerView for the jobs in other locations
        recyclerView2 = (RecyclerView)rootView.findViewById(R.id.recyclerView2);
        recyclerView2.setHasFixedSize(true);
        recyclerView2.setLayoutManager(new LinearLayoutManager(getActivity()));

        //Initializes the RecyclerView for the jobs in progress
        recyclerView3 = (RecyclerView)rootView.findViewById(R.id.recyclerViewAccepted);
        recyclerView3.setHasFixedSize(true);
        recyclerView3.setLayoutManager(new LinearLayoutManager(getActivity()));

        //Initialize the process dialog
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading Data");
        progressDialog.show();


        //Grab the id of the current user
        auth = FirebaseAuth.getInstance();
        id = auth.getCurrentUser().getUid();

        //Initialize the database reference
        databaseReference = FirebaseDatabase.getInstance().getReference("job");
        databaseReference.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot snapshot)
            {
                //Grabs each element in the relevant database
                for (DataSnapshot dataSnapshot : snapshot.getChildren())
                {
                    //Grabs the current UserJob object
                    UserJob uj = dataSnapshot.getValue(UserJob.class);
                    uj.setDbRef(dataSnapshot.getKey());
                    Log.d(TAG, "onDataChange: userjob : " + uj.getTitle());

                    //Checks if the location of the pickup address is null
                    if(uj.getPAdd().getLoc() != null)
                    {
                        //If it isn't sets the latitude and longitude of the address to the pickup
                        //location object
                        Log.d(TAG, "User Job : " + uj.getPAdd().getPostcode());
                        pickup.setLatitude(uj.getPAdd().getLoc().getLatitude());
                        pickup.setLongitude(uj.getPAdd().getLoc().getLongitude());
                    }

                    //Checks if the location of the delivery address is null
                    if(uj.getDAdd().getLoc() != null)
                    {
                        //If it isn't sets the latitude and longitude of the address to the delivery
                        //location object
                        dropoff.setLatitude(uj.getDAdd().getLoc().getLatitude());
                        dropoff.setLongitude(uj.getDAdd().getLoc().getLongitude());
                    }


                    if(uj.getStatus().equals("Accepted"))
                    {
                        pList.add(uj);
                    }
                    else if(!uj.getStatus().equals("Completed"))
                    {
                        if(!(id.equals(uj.getPoster())))
                        {
                            //If it isn't then checks if the pickup location is null
                            if(pickup != null)
                            {
                                //If it isn't then checks if the dropoff location is null
                                if(dropoff != null)
                                {
                                    //If it isn't finds the distance between the current location and the pickup or dropoff
                                    if(current.distanceTo(pickup) < range || current.distanceTo(dropoff) < range)
                                    {
                                        //If either the pickup location of the dropoff location are within range
                                        //the job is added to those in the current location
                                        aList.add(uj);
                                    }
                                    else
                                    {
                                        //If neither are in range, then the job is added to other jobs
                                        oList.add(uj);
                                    }
                                }

                                else
                                {
                                    //If dropoff is null, job added to other jobs
                                    oList.add(uj);
                                }
                            }
                            else
                            {
                                //If pickup is null, job added ot other jobs
                                oList.add(uj);
                            }
                        }
                    }

                    //Re-initialize locations
                    pickup = new Location("");
                    dropoff = new Location("");

                }

                //Sets the adapter for the current location jobs and passes the jobs to the adapter
                adapter = new JobsRecyclerViewAdapter(getActivity(), aList);
                recyclerView.setAdapter(adapter);

                //Sets the adapter for the other jobs and passes the jobs to the adapter
                adapter2 = new JobsRecyclerViewAdapter(getActivity(), oList);
                recyclerView2.setAdapter(adapter2);

                //Sets the adapter for the jobs in progress and passes the jobs to the adapter
                adapter3 = new JobsRecyclerViewAdapter(getActivity(), pList);
                recyclerView3.setAdapter(adapter3);

                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                //If database error occurs, then jobs won't load
                progressDialog.dismiss();

            }
        });

        return rootView;
    }

    /**
     * Gets the current location of the device
     */
    public void getDeviceLocation()
    {
        Log.d(TAG, "getDeviceLocation: getting the device's current location");

        //Initializes the location provider and a new geocoder
        mfusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getContext());
        final Geocoder gcd = new Geocoder(getContext());

        try
        {
            //Checks if location permissions have been granted
            if(mLocationPermissionGranted)
            {
                //If they have, grabs the last known location of the device
                Task location = mfusedLocationProviderClient.getLastLocation();

                //onCompleteListener for location task
                location.addOnCompleteListener(new OnCompleteListener()
                {
                    @Override
                    public void onComplete(@NonNull Task task)
                    {
                        //Checks if getting the last known location was successful
                        if(task.isSuccessful())
                        {
                            //If it was, then it grabs the result of the task and passes the latitude
                            //and longitude to the current location object
                            Log.d(TAG, "onComplete: found location!");
                            Location currentLocation = (Location)task.getResult();
                            Log.d(TAG, "Current Latitude : " + currentLocation.getLatitude());
                            Log.d(TAG, "Current Longitude : " + currentLocation.getLongitude());
                            current.setLatitude(currentLocation.getLatitude());
                            current.setLongitude(currentLocation.getLongitude());

                            try
                            {
                                //Grabs the geocoder location from the current location
                                addresses = gcd.getFromLocation(current.getLatitude(),current.getLongitude(),10);
                            }
                            catch (IOException e)
                            {
                                e.printStackTrace();
                            }


                            //Grabs each element in the relevant database
                            for(Address address : addresses)
                            {
                                //Checks if the locality or postal code is null
                                if(address.getLocality() != null && address.getPostalCode() != null)
                                {
                                    //If neither are null then it grabs the post code of the current location
                                    currentPostalCode = address.getPostalCode();
                                }
                            }


                        }
                        else
                        {
                            //If the task was unsuccessful then it informs the user that the app was
                            //unable to get the current location
                            Log.d(TAG, "onComplete: current location is null");
                            Toast.makeText(getContext(), "Unable to get current location", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }
        catch(SecurityException e)
        {
            Log.d(TAG, "getDeviceLocation: SecurityException: " + e.getMessage());

        }
    }

    /**
     * Checks that Google Location Services are working
     * @return if services are ok
     */
    public boolean isServicesOK()
    {
        Log.d(TAG, "isServicesOK: checking google services version");
        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getContext());
        if(available == ConnectionResult.SUCCESS)
        {
            // everything is fine and user can make map requests
            Log.d(TAG, "isServicesOK: Google Play Services is working");
            return true;
        }
        else if(GoogleApiAvailability.getInstance().isUserResolvableError(available))
        {
            // an error occurred but we can resolve it
            Log.d(TAG, "isServicesOK: an error occured but we can fix it");
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(getActivity(),available,ERROR_DIALOG_REQUEST);
            dialog.show();
        }
        else
        {
            Toast.makeText(getContext(), "You can't make map requests", Toast.LENGTH_SHORT).show();
        }
        return false;
    }


    /**
     * Checks that the app has permission to access the user's location
     */
    private void getLocationPermission()
    {
        Log.d(TAG, "getLocationPermission: getting location permissions");
        String [] permissions = {android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION};

        if(ContextCompat.checkSelfPermission(getContext(), FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            if(ContextCompat.checkSelfPermission(getContext(), COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            {
                mLocationPermissionGranted = true;
            }
            else
            {
                ActivityCompat.requestPermissions(getActivity(), permissions, LOCATION_PERMISSION_REQUEST_CODE);
            }
        }

    }

    /**
     * Checks the result of the app permissions
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: called.");
        mLocationPermissionGranted = false;
        switch(requestCode)
        {
            case LOCATION_PERMISSION_REQUEST_CODE:
            {
                if(grantResults.length > 0)
                {

                    for(int i = 0; i <grantResults.length; i++)
                    {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED)
                        {
                            mLocationPermissionGranted = false;
                            Log.d(TAG, "onRequestPermissionsResult: permission failed");
                            return;
                        }
                    }

                    Log.d(TAG, "onRequestPermissionsResult: permission granted");
                    mLocationPermissionGranted = true;
                }
            }
        }
    }
}
