package uk.ac.tees.com2060.cupcake;


import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

/**
 * JobDetailsActivity Class
 */
public class JobDetailsActivity extends AppCompatActivity
{
    //Variables for layout access
    ViewPagerAdapter viewPagerAdapter;
    ViewPager viewPager;
    TabLayout tabLayout;
    Toolbar toolbar;

    /**
     * Initializes the Activity
     * Retrieves any required layout widgets
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        //Sets up the required xml layout file
        setContentView(R.layout.activity_job_details);

        //Initializes the toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Create the adapter that will return a fragment for each of the three primary sections
        //of the activity
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        //Set up the ViewPager with the sections adapter and the TabLayout
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout = (TabLayout) findViewById(R.id.vTabs);

        //Sets action listeners to viewPager and tabLayout
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));

    }

    /**
     * Initializes the contents of the menu options provided
     * @param menu
     * @return true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_job_details, menu);
        return true;
    }

    /**
     * Called when a menu option is selected and executes the relevant calls
     * @param item
     * @return menu item selected
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class ViewPagerAdapter extends FragmentPagerAdapter
    {

        /**
         * Main constructor
         * @param fm
         */
        public ViewPagerAdapter(FragmentManager fm)
        {
            super(fm);
        }

        /**
         * Moves to selected tab
         * @param position
         * @return selected tab or null
         */
        @Override
        public Fragment getItem(int position)
        {
            //Finds the tab at the current position and returns a new instance of the selected tab
            switch (position)
            {
                case 0:
                    JobDetailsTab1View tab1 = new JobDetailsTab1View();
                    return tab1;
                case 1:
                    JobDetailsTab2Review tab2 = new JobDetailsTab2Review();
                    return tab2;
                default:
                    return null;
            }
        }

        /**
         * Returns the number of tabs
         * @return 2
         */
        @Override
        public int getCount()
        {
            // Show 2 total pages.
            return 2;
        }

        /**
         * Grabs the title of the current tab
         * @param position
         * @return title
         */
        @Override
        public CharSequence getPageTitle(int position)
        {
            String title = null;

            //Uses the current position to find the tab title
            if (position == 0)
            {
                title = "View Job";
            }
            else if (position == 1)
            {
                title = "Reviews";
            }

            return title;
        }
    }
}
