package uk.ac.tees.com2060.cupcake;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;

/**
 * Profile Pic Dialog activity.
 */

public class ProfilePicDialog extends AppCompatDialogFragment
{
    //Variables for layout access
    ImageButton btnCamera, btnGallery;

    /**
     *  Initializes and returns a Dialog class.
     * @param savedInstanceState
     * @return builder.create()
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_reviewdialog, null);

        builder.setView(view);

        btnCamera = view.findViewById(R.id.btn_camera);
        btnGallery = view.findViewById(R.id.btn_gallery);

        btnCamera.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {}
        });

        return builder.create();
    }

}
