package uk.ac.tees.com2060.cupcake;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

/**
 * ReviewDialog Class
 */
public class ReviewDialog extends AppCompatDialogFragment
{
    //Variables for layout access
    EditText txtReview, txtRating;

    //Variable for dialog listener
    ReviewDialogListener listener;

    /**
     * Initializes and returns a Dialog class
     * @param savedInstanceState
     * @return builder.create()
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        //Initializes the builder
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        //Sets up the required xml layout file
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_reviewdialog, null);

        //Initialize widgets
        txtReview = view.findViewById(R.id.txt_review);
        txtRating = view.findViewById(R.id.txt_rating);

        //Set the builder's view
        builder.setView(view)
                //Initialize dialog buttons
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {

                    }
                })
                .setPositiveButton("Done", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {   //Grabs the inputs from the edittexts
                        String review = txtReview.getText().toString();
                        String rating = txtRating.getText().toString();

                        //Sends the values to the main activity
                        listener.sendTextFields(review, rating);
                    }
                });

        return builder.create();
    }

    /**
     * Called when Dialog is first attached to it's context
     * @param context
     */
    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        try
        {
            listener = (ReviewDialogListener) context;
        }
        catch (ClassCastException e)
        {
            //A reminder to implement the listener to all classes it's needed during development.
            throw new ClassCastException(context.toString() + " must implements ReviewDialogListener!!");
        }
    }

    /**
     * Interface for ReviewDialogListener
     */
    public interface ReviewDialogListener
    {
        void sendTextFields(String review, String rating);
    }
}
