package uk.ac.tees.com2060.cupcake;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.support.v7.app.ActionBar;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import com.google.firebase.auth.FirebaseUser;
import java.util.List;

/**
 * Settings Activity.
 *
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends AppCompatPreferenceActivity
{
    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener()
    {
        /**
         *
         * @param preference
         * @param value
         * @return
         */
        @Override
        public boolean onPreferenceChange(Preference preference, Object value)
        {
            String stringValue = value.toString();

                // For all other preferences, set the summary to the value's
                // simple string representation.
                preference.setSummary(stringValue);

            return true;
        }
    };

    /**
     * Helper method to determine if the device has an extra-large screen. For
     * example, 10" tablets are extra-large.
     * * @param context
     * @return if screen is okay
     */
    private static boolean isXLargeTablet(Context context)
    {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     * @param preference
     * @see #sBindPreferenceSummaryToValueListener
     * */
    private static void bindPreferenceSummaryToValue(Preference preference)
    {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }

    /**
     * Initializes the activity.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setupActionBar();
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar()
    {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
        {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * {@inheritDoc}
     * @return if it is multiPane
     */
    @Override
    public boolean onIsMultiPane() {
        return isXLargeTablet(this);
    }


    /**
     * {@inheritDoc}
     * @param target
     */
    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onBuildHeaders(List<Header> target)
    {
        loadHeadersFromResource(R.xml.pref_headers, target);
    }

    /**
     * This method stops fragment injection in malicious applications.
     * Make sure to deny any unknown fragments here.
     * @param fragmentName
     * @return if it is a valid fragment
     */
    protected boolean isValidFragment(String fragmentName)
    {
        return PreferenceFragment.class.getName().equals(fragmentName)
                || AccountSettingsPreferenceFragment.class.getName().equals(fragmentName)
                || TermsPreferenceFragment.class.getName().equals(fragmentName)
                || HelpContactPreferenceFragment.class.getName().equals(fragmentName);
    }

    /**
     * The account settings preference fragment.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class AccountSettingsPreferenceFragment extends PreferenceFragment
    {
        FirebaseUser user;

        /**
         * Initializes the account settings xml file.
         * @param savedInstanceState
         */
        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_account);
            setHasOptionsMenu(true);

            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.
            //bindPreferenceSummaryToValue(findPreference("email"));

        }

        /**
         * The method to handle click events.
         * You can identify the item by calling getItemId() as it gives the unique ID.
         * This ID is then matched against android.R.id.home and then the action to start the activity is called.
         * If the ID is not matched then a superclass implementation of onOptionsItemSelected is called.
         * @param item
         * @return either true or if an item is selected
         */
        @Override
        public boolean onOptionsItemSelected(MenuItem item)
        {
            int id = item.getItemId();
            if (id == android.R.id.home)
            {
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
    }

    /**
     * The terms and conditions settings preference fragment.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class TermsPreferenceFragment extends PreferenceFragment
    {
        /**
         * Initializes the terms settings xml file.
         * @param savedInstanceState
         */
        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_terms_conditions);
            setHasOptionsMenu(true);

            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.
            bindPreferenceSummaryToValue(findPreference("terms1"));
        }

        /**
         * The method to handle click events.
         * You can identify the item by calling getItemId() as it gives the unique ID.
         * This ID is then matched against android.R.id.home and then the action to start the activity is called.
         * If the ID is not matched then a superclass implementation of onOptionsItemSelected is called.
         * @param item
         * @return true or if item is selected
         */
        @Override
        public boolean onOptionsItemSelected(MenuItem item)
        {
            int id = item.getItemId();
            if (id == android.R.id.home)
            {
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
    }

    /**
     * The help settings preference fragment.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class HelpContactPreferenceFragment extends PreferenceFragment
    {
        /**
         * Initializes the help and contact settings xml file.
         * @param savedInstanceState
         */
        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_help_contact);
            setHasOptionsMenu(true);

            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.
            bindPreferenceSummaryToValue(findPreference("help1"));
        }

        /**
         * The method to handle click events.
         * You can identify the item by calling getItemId() as it gives the unique ID.
         * This ID is then matched against android.R.id.home and then the action to start the activity is called.
         * If the ID is not matched then a superclass implementation of onOptionsItemSelected is called.
         * @param item
         * @return true or if item is selected
         */
        @Override
        public boolean onOptionsItemSelected(MenuItem item)
        {
            int id = item.getItemId();
            if (id == android.R.id.home)
            {
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
    }
}
