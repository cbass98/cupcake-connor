package uk.ac.tees.com2060.cupcake;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * SetupActivity Class
 */
public class SetupActivity extends AppCompatActivity
{
    //Variables for layout access
    EditText etName, etPhone, etNumber, etStreet, etAdd, etCity, etCounty, etPostcode;
    Button btnCreate;
    ProgressBar progressbar;

    //Variable for object storage
    String id;

    //Variable for Firebase access
    FirebaseAuth auth;

    /**
     * Initializes Activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);

        //Initialize widgets
        etName = (EditText)findViewById(R.id.et_name);
        etPhone= (EditText)findViewById(R.id.et_phone);
        etNumber = (EditText)findViewById(R.id.et_number);
        etStreet = (EditText)findViewById(R.id.et_street);
        etAdd = (EditText)findViewById(R.id.et_additional);
        etCity = (EditText)findViewById(R.id.et_city);
        etCounty = (EditText)findViewById(R.id.et_county);
        etPostcode = (EditText)findViewById(R.id.et_postcode);
        btnCreate = (Button)findViewById(R.id.create);
        progressbar = (ProgressBar) findViewById(R.id.progressBar);

        //Grabs the id of the current user
        auth = FirebaseAuth.getInstance();
        id = auth.getUid();

        //Initialize database reference
        final DatabaseReference uDatabase = FirebaseDatabase.getInstance().getReference("user");

        //OnClickListener for button
        btnCreate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Grabs the inputs from the editexts
                String name = etName.getText().toString();
                String phone = etPhone.getText().toString();
                JAddress address = new JAddress(Integer.parseInt(etNumber.getText().toString()), etStreet.getText().toString(),
                        etAdd.getText().toString(),etCity.getText().toString(),etCounty.getText().toString(),etPostcode.getText().toString(), null);

                //Creates a new User object and pushes it up to the database
                User user = new User(id, name, phone, address, auth.getCurrentUser().getEmail());
                uDatabase.child(id).setValue(user);

                //Loads the ExploreActivity
                startActivity(new Intent(SetupActivity.this,ExploreActivity.class));
                finish();
            }
        });
    }
}
