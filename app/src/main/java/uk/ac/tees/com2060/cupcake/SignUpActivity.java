package uk.ac.tees.com2060.cupcake;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class SignUpActivity extends AppCompatActivity
{
    //Variables for layout access
    EditText inputEmail, inputPassword;
    Button btnSignIn, btnSignUp;
    ImageView picChange;
    ProgressBar progressbar;

    //Variable for Firebase access
    FirebaseAuth auth;

    /**
     * Initializes Activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        //Initialize widgets
        picChange = (ImageView) findViewById(R.id.pic_change);
        btnSignIn = (Button) findViewById(R.id.sign_in_button);
        btnSignUp = (Button) findViewById(R.id.sign_up_button);
        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        progressbar = (ProgressBar) findViewById(R.id.progressBar);

        //Gets current instance of Firebase
        auth = FirebaseAuth.getInstance();

        //OnClickListener's for ImageView and Buttons
        picChange.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Calls openProfileDialog
                openProfileDialog();
            }
        });

        btnSignIn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Loads up the LoginActivity class.
                startActivity(new Intent(SignUpActivity.this,LoginActivity.class));
                finish();
            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Grabs the inputs from the editTexts
                String email = inputEmail.getText().toString();
                String password = inputPassword.getText().toString();

                //Checks if the inputs are null
                if(TextUtils.isEmpty(email))
                {
                    Toast.makeText(getApplicationContext(), "Enter email address!",Toast.LENGTH_SHORT).show();
                }

                if(TextUtils.isEmpty(password))
                {
                    Toast.makeText(getApplicationContext(),"Enter password!", Toast.LENGTH_SHORT).show();
                }

                //Checks that the password is of a suitable length
                if(password.length()< 8)
                {
                    Toast.makeText(getApplicationContext(),"Password too short, enter a minimum of 8 characters!", Toast.LENGTH_SHORT).show();
                }

                //Shows when signing up
                progressbar.setVisibility(View.VISIBLE);

                //Creates new Firebase User
                auth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(SignUpActivity.this, new OnCompleteListener<AuthResult>()
                {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task)
                    {
                        //Prints out action message
                        Toast.makeText(SignUpActivity.this, "createuserWithEmail:onComplete:" + task.isSuccessful(),Toast.LENGTH_SHORT).show();

                        //Shows while creating account
                        progressbar.setVisibility(View.GONE);

                        //Checks if creation was successful and prints out relevant message
                        if(!task.isSuccessful())
                        {
                            Toast.makeText(SignUpActivity.this, "Authenitcation failed." + task.getException(), Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            //Loads SetupActivity
                            startActivity(new Intent(SignUpActivity.this,SetupActivity.class));
                            finish();
                        }
                    }
                });
            }
        });
    }

    /**
     * Open's profile dialog
     */
    public void openProfileDialog()
    {
        //Creates new dialog and shows
        ProfilePicDialog profilePicDialog = new ProfilePicDialog();
        profilePicDialog.show(getSupportFragmentManager(), "ProPic Dialog");
    }

    /**
     * On Resume
     */
    @Override
    protected void onResume()
    {
        super.onResume();
        progressbar.setVisibility(View.GONE);
    }
}