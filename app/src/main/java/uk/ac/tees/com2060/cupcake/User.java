package uk.ac.tees.com2060.cupcake;


import java.io.Serializable;

import uk.ac.tees.com2060.cupcake.JAddress;

/**
 * Model class used to represent a single user
 */
public class User implements Serializable
{
    /**
     * Attributes of User
     */
    private String id, name, phone, email, image;
    private uk.ac.tees.com2060.cupcake.JAddress JAddress;

    /**
     * Empty constructor
     */
    public User(){}

    /**
     * Main constructor
     * @param i
     * @param n
     * @param p
     * @param a
     * @param e
     */
    public User(String i,String n, String p, JAddress a, String e)
    {
        id = i;
        name = n;
        phone = p;
        JAddress = a;
        email = e;
    }

    /**
     * Retrieves the user id
     * @return id
     */
    public String getId()
    {
        return id;
    }

    /**
     * Retrieves the user name
     * @return name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Retrieves the user's phone number
     * @return phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Retrieves the user's address
     * @return JAddress
     */
    public JAddress getJAddress() {
        return JAddress;
    }

    /**
     * Retrieves the user's email
     * @return email
     */
    public String getEmail()
    {
        return email;
    }

    /**
     * Retrieves the user's image
     * @return image
     */
    public String getImage()
    {
        return image;
    }

    /**
     * Sets the image
     * @param i
     */
    public void setImage(String i)
    {
        image = i;
    }
}
