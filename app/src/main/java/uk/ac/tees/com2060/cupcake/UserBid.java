package uk.ac.tees.com2060.cupcake;

import java.io.Serializable;

/**
 * Model class used to represent a single bid
 */
public class UserBid implements Serializable
{
    /**
     * Attributes for UserBid
     */
    String jobRef;
    String bidder;
    double bid;
    boolean accepted;

    /**
     * Empty constructor
     */
    public UserBid(){}

    /**
     * Main constructor
     * @param j
     * @param bi
     * @param b
     */
    public UserBid(String j, String bi, double b, boolean a)
    {
        jobRef = j;
        bidder = bi;
        bid = b;
        accepted = a;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    /**
     * Retrieves the user's bid value
     * @return bid
     */
    public double getBid() {
        return bid;
    }

    /**
     * Retrieves the user's id who bidded
     * @return bidder
     */
    public String getBidder() {
        return bidder;
    }

    /**
     * Retrieves the reference to job that was bid on
     * @return jobRed
     */
    public String getJobRef() {
        return jobRef;
    }

    public Boolean getAccepted()
    {
        return accepted;
    }
}
