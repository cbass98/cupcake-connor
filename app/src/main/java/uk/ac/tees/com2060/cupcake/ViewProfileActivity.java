package uk.ac.tees.com2060.cupcake;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.ContentValues.TAG;

/**
 * ViewProfileActivity Class
 */
public class ViewProfileActivity extends AppCompatActivity implements ReviewDialog.ReviewDialogListener
{
    //Variable for database reference
    DatabaseReference rDatabase;

    //Variables for layout access
    TextView name;
    CircleImageView image;
    Button btnReview, btnFeedback;

    //Variable for Firebase access
    FirebaseAuth auth;

    //Variable for object storage
    String reviewID, reviewer, reviewee;
    Review rev;
    User u;

    /**
     * Initializes Activity
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        //Sets the required xml layout file
        setContentView(R.layout.activity_view_profile);

        //Calls getIncomingIntent()
        getIncomingIntent();

        //Initialize widgets
        btnReview = (Button) findViewById(R.id.btn_review);
        btnFeedback = (Button) findViewById(R.id.btn_feedback);
        image = (CircleImageView)findViewById(R.id.profile_image);
        name = (TextView) findViewById(R.id.tv_name);

        btnReview.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Opens the review dialog
                openReviewDialog();
            }
        });

        btnFeedback.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {   //Loads the AllFeedbackActivity and passes the user
                Intent intent = new Intent(ViewProfileActivity.this, AllFeedbackActivity.class);
                intent.putExtra("user",u);
                startActivity(intent);
            }
        });

        //Sets the display name and the profile image
        name.setText(u.getName());
        Picasso.with(this).load(u.getImage()).placeholder(R.mipmap.ic_launcher).into(image);


    }

    /**
     * Creates a new ReviewDialog
     */
    public void openReviewDialog()
    {
        ReviewDialog reviewDialog = new ReviewDialog();
        reviewDialog.show(getSupportFragmentManager(), "review dialog");
    }

    /**
     * Retrieves text fields from the review dialog
     * @param review
     * @param rating
     */
    @Override
    public void sendTextFields(String review, String rating)
    {
        //Grabs the id of the current user
        auth = FirebaseAuth.getInstance();
        reviewer = auth.getCurrentUser().getUid();

        //Grabs the id of the current user object
        reviewee = u.getId();

        //Initializes the database reference
        rDatabase = FirebaseDatabase.getInstance().getReference("review");

        //Pushes the new review object to the database
        reviewID = rDatabase.push().getKey();
        rev = new Review(review, rating, reviewer, reviewee);
        rDatabase.child(reviewID).setValue(rev);
    }

    /**
     * Retrieves data sent with the intent in the extra field
     */
    private void getIncomingIntent(){
        Log.d(TAG, "getIncomingIntent: checking for incoming intent");

        //Checks if the intent has an extra with the reference job
        if(this.getIntent().hasExtra("poster"))
        {
            Log.d(TAG, "getIncomingIntent: found intent extra");

            //Grabs the data in the extra
            u = (User) this.getIntent().getSerializableExtra("poster");
        }
    }
}